from django.urls import path

from . import views

urlpatterns = [
    path('cost/<int:cost_less_than>', views.cost_per_acquisition, name='cost'),
    path('fallen/<int:percent>', views.fallen, name='fallen'),
]