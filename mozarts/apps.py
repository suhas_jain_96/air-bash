from django.apps import AppConfig


class MozartsConfig(AppConfig):
    name = 'mozarts'
