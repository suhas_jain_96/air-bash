from datetime import datetime
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from mozarts.models import dataset1,dataset2

# Create your views here.
def cost_per_acquisition(request,cost_less_than=500):
	data=dataset1.objects.all()
	result=[]
	for da in data:
		if(da.Cost_per_sale<cost_less_than):
			result.append(da.Dimension)
	return JsonResponse({'result':result}, safe=False)
def fallen(request,percent=20):
	data=dataset2.objects.all()
	data=sorted(data,key=lambda x:(x.Campaign_name,datetime.strptime(x.Date, '%d %b %Y')))
	print(data)
	previous_data=dataset2()
	result=[]
	for da in data:
		print(da)
		if(da.Campaign_name == previous_data.Campaign_name):
			print("\n\n\n\n")
			print(da)
			if(previous_data.Sales !=0):
				fall=((int(previous_data.Sales)-int(da.Sales))/previous_data.Sales)*100
			else:
				fall=100
			if(fall>percent):
				result.append([previous_data.Campaign_name,previous_data.Date])
		previous_data=da
	return JsonResponse({'result':result}, safe=False)