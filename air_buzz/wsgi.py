"""
WSGI config for air_buzz project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/howto/deployment/wsgi/
"""
import runpy
import os
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))#django folder
activate_this = os.path.join(BASE_DIR,'en/bin/activate_this.py')
runpy.run_path(activate_this)


import sys
#import logging
#logging.basicConfig(stream=sys.stderr)
sys.path.insert(0,os.path.join(BASE_DIR,"air_buzz"))
from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'air_buzz.settings')

application = get_wsgi_application()
