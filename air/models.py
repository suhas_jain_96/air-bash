from django.db import models

# Create your models here.
class Airbus(models.Model):
        """docstring for Airbus"""

        MSN=models.IntegerField(unique=True)
        Type=models.CharField(max_length=10)
        Harness_length=models.IntegerField()
        Gross_weight=models.IntegerField()
        Atmospheric_pressure=models.IntegerField()
        Room_temp=models.IntegerField()
        Airport=models.CharField(max_length=50)
        Fuel_cap_LW=models.IntegerField()
        Fuel_cap_RW=models.IntegerField()
        Fuel_quant_LW=models.IntegerField()
        Fuel_quant_RW=models.IntegerField()
        Max_altitude=models.IntegerField()
        Flight_no=models.CharField(max_length=20)

