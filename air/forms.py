from django.forms import ModelForm, Form
from air.models import Airbus
from django import forms

class AirbusForm(ModelForm):
    # your_name = forms.CharField(label='Your name', max_length=100)
 #    MSN = forms.IntegerField()
 #    Type = forms.forms.CharField()
	# Harness_length = forms.IntegerField()
	# Gross_weight = forms.IntegerField()
	# Atmospheric_pressure = forms.IntegerField()
	# Room_temp = forms.IntegerField()
	# Airport = forms.CharField()
	# Fuel_cap_LW = forms.IntegerField()
	# Fuel_cap_RW = forms.IntegerField()
	# Fuel_quant_LW = forms.IntegerField()
	# Fuel_quant_RW = forms.IntegerField()
	# Max_altitude = forms.IntegerField()
	# Flight_no = forms.CharField()
	class Meta(object):
		model = Airbus
		fields = ['MSN', 'Harness_length', 'Gross_weight', 'Atmospheric_pressure', 'Room_temp', 'Airport', 'Fuel_cap_LW', 'Fuel_cap_RW', 'Fuel_quant_LW', 'Fuel_quant_RW', 'Max_altitude', 'Flight_no']
		labels = {
			'MSN' : "Manufacturer Serial Number",
			'Harness_length': "Harness Length",
			'Gross_weight' : "Gross Weight",
			'Atmospheric_pressure' : "Atmospheric Pressure",
			'Room_temp' : "Room Temperature",
			'Airport' : "Airport",
			'Fuel_cap_LW' : "Fuel Capacity on Left Wing",
			'Fuel_cap_RW' : "Fuel Capacity on Right Wing",
			'Fuel_quant_LW' : "Fuel Quantity on Left Wing",
			'Fuel_quant_RW' : "Fuel Quantity on Right Wing",
			'Max_altitude' : "Maximum Altitude",
			'Flight_no' : "Flight Number"

		}
		widgets = {
			'MSN' : forms.TextInput(attrs={'class':'form-control','placeholder':'Enter Manufacturer Serial Number'}),
			'Harness_length' : forms.TextInput(attrs={'class':'form-control','placeholder':'Enter Harness Length'}),
			'Gross_weight' : forms.TextInput(attrs={'class':'form-control','placeholder':'Enter Gross Weight'}),
			'Atmospheric_pressure' : forms.TextInput(attrs={'class':'form-control','placeholder':'Enter Atmospheric Pressure'}),
			'Room_temp' : forms.TextInput(attrs={'class':'form-control','placeholder':'Enter Room Temperature'}),
			'Airport' : forms.TextInput(attrs={'class':'form-control','placeholder':'Enter Name of Airport'}),
			'Fuel_cap_LW' : forms.TextInput(attrs={'class':'form-control','placeholder':'Enter Fuel Capacity on Left Wing'}),
			'Fuel_cap_RW' : forms.TextInput(attrs={'class':'form-control','placeholder':'Enter Fuel Capacity on Right Wing'}),
			'Fuel_quant_LW' : forms.TextInput(attrs={'class':'form-control','placeholder':'Enter Fuel Quantity on Left Wing'}),
			'Fuel_quant_RW' : forms.TextInput(attrs={'class':'form-control','placeholder':'Enter Fuel Quantity on Right Wing'}),
			'Max_altitude' : forms.TextInput(attrs={'class':'form-control','placeholder':'Enter Maximum Altitude'}),
			'Flight_no' : forms.TextInput(attrs={'class':'form-control','placeholder':'Enter Flight Number'}),
		}
