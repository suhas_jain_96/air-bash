from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('upload/<str:typ>', views.upload, name='upload'),
    path('thanks/', views.thanks, name='thanks'),
    # path('up/<str:typ>',views.up ,name='up'),
    path('search',views.search, name='search')
]
